package src.validadorRut;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class validadorRut{

    private static final Pattern RUT_PATTERN = Pattern.compile("^0*(\\d{1,3}(\\.\\d{3})*)-([\\dKk])$");

    public static boolean isValid(String rut) {
        if (rut == null || !RUT_PATTERN.matcher(rut).matches()) {
            return false;
        }

        String[] parts = rut.split("-");
        String number = parts[0].replace(".", "");
        char verificador = Character.toUpperCase(parts[1].charAt(0));

        return verificador == calculateDV(number);
    }

    private static char calculateDV(String rut) {
        int M = 0, S = 1, T = Integer.parseInt(rut);

        for (; T != 0; T = (int) (double) (T /= 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;

        return (char) (S > 0 ? S + 47 : 75);
    }
}

