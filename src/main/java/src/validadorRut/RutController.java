package src.validadorRut;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RutController {
    private final validadorRut rutValidator;

    @Autowired
    public RutController(validadorRut rutValidator) {
        this.rutValidator = rutValidator;
    }

    @GetMapping({"/validateRut"})
    public String validateRut() {
        String rut1 = "20.794.479-3";
        String rut2 = "13.4k.678-0";
        validadorRut var10001 = this.rutValidator;
        String var3 = validadorRut.isValid(rut1) ? "válido" : "inválido";
        validadorRut var10003 = this.rutValidator;
        return "El RUT " + rut1 + " es " + var3 + ", y el RUT " + rut2 + " es " + (validadorRut.isValid(rut2) ? "válido" : "inválido");
    }
}

